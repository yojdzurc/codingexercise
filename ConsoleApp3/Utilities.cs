﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using Microsoft.MapPoint;
using NLog;

namespace LightningAlertConsoleApp
{
    public static class Utilities
    {


        public const int lowestLevel = 0;
        public const int highestLevel = 23;
        static readonly Logger log = LogManager.GetCurrentClassLogger();


        /// <summary>
        /// Deserialize Json File of a specified type
        /// </summary>
        /// <param name="path">location of the json file</param>
        /// <param name="dataType">expected datatype to be deserialized</param>
        /// <returns></returns>
        public static object Deserialize(string path, Type dataType)
        {
            object returnObj = new object();
            if (!File.Exists(path))
            {
                log.Error("{0} does not exists.", path);
                return null;
            }
                try
                {
                    var jsonOptions = new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true,
                        AllowTrailingCommas = true,
                    };

                    if (dataType == typeof(List<LightningStrike>))
                    {
                        List<LightningStrike> listOfLightning = new List<LightningStrike>();

                        using (StreamReader reader = File.OpenText(path))
                        {
                            while (!reader.EndOfStream)
                            {
                                string line = reader.ReadLine();
                                LightningStrike lightningModel = new LightningStrike();
                                try
                                {
                                    lightningModel = JsonSerializer.Deserialize<LightningStrike>(line, jsonOptions);
                                }
                                catch (JsonException je)
                                {
                                    //Console.WriteLine("Error encountered in parsing an item.");
                                    log.Error(string.Format("Error in parsing {0}. : {1}.", path, je.Message));
                                    continue;
                                }
                                listOfLightning.Add(lightningModel);
                            }
                        }
                        returnObj = listOfLightning;
                    }
                    else if (dataType == typeof(List<Asset>))
                    {
                        string jsonContent = String.Empty;
                        if (path != string.Empty)
                            jsonContent = File.ReadAllText(path);
                        returnObj = JsonSerializer.Deserialize<List<Asset>>(jsonContent, jsonOptions);

                    }

                }
                catch (JsonException je)
                {
                    log.Error(string.Format("Error in parsing {0}. :  {1}", path, je.Message));
                    throw;
                }
                catch (IOException ie) 
                {
                    log.Error(string.Format("Error in accessing {0}. :  {1}", path, ie.Message));
                    throw;
                }
                catch (Exception e)
                {
                    log.Error(string.Format("Error in deserializing {0}. :  {1}", path, e.Message));
                    throw;
                }
            
            return returnObj;
        }

        /// <summary>
        /// Converts a point from latitude/longitude WGS-84 coordinates (in degrees) 
        /// into a QuadKey at a specified level of detail
        /// </summary>
        /// <param name="level">Level of detail, from 1 (lowest detail)  
        /// to 23 (highest detail).</param> 
        /// <param name="latitude">Latitude of the point, in degrees.</param>  
        /// <param name="longitude">Longitude of the point, in degrees.</param> 
        /// <returns></returns>
        public static string LatLongToQuadKey(int level, double latitude, double longitude) 
        {
            if ((level <= lowestLevel) || (level > highestLevel))
            {
                log.Error("Level of Detail must be from 1 to 23");
                return null;
            }

            TileSystem.LatLongToPixelXY(latitude, longitude, level, out int pixX, out int pixY);
            TileSystem.PixelXYToTileXY(pixX, pixY, out int tilX, out int tilY);
            return TileSystem.TileXYToQuadKey(tilX, tilY, level);
            

        } 
    }  
}
