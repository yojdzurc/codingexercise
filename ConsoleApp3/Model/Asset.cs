﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightningAlertConsoleApp
{
    public class Asset
    {
        public string AssetName { get; set; }
        public string QuadKey { get; set; }
        public string AssetOwner { get; set; }

    }
}
