﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace LightningAlertConsoleApp
{
    class Program
    {
        static readonly Logger log = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            try
            {

                string lightningPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\..\..\Data\lightning.json"));
                if (!File.Exists(lightningPath))
                {
                    string lightningFilename = Path.GetFileName(lightningPath);
                    Console.WriteLine(string.Format("{0} does not exist.", lightningFilename));
                    log.Error(string.Format("{0} does not exist.", lightningFilename));
                    Console.ReadLine();
                    return;
                }

                string assetPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\..\..\Data\assets.json"));
                if (!File.Exists(assetPath))
                {
                    string assetsFilename = Path.GetFileName(assetPath);
                    Console.WriteLine(string.Format("{0} does not exist.", assetsFilename));
                    log.Error(string.Format("{0} does not exist.", assetsFilename));
                    Console.ReadLine();
                    return;
                }

                var listOfLightningStrikes = new List<LightningStrike>();
                listOfLightningStrikes = (List<LightningStrike>)Utilities.Deserialize(lightningPath, typeof(List<LightningStrike>));

                var listOfAssets = new List<Asset>();
                listOfAssets = (List<Asset>)Utilities.Deserialize(assetPath, typeof(List<Asset>));
                int level = 12;
                HashSet<string> alerted = new HashSet<string>();

                var watch = System.Diagnostics.Stopwatch.StartNew();
                foreach (var light in listOfAssets)
                {
                    foreach (var item in listOfLightningStrikes)
                    {
                        if (item.FlashType != (int)FlashType.Heartbeat)
                        {
                            if (Utilities.LatLongToQuadKey(level, item.Latitude, item.Longitude) == light.QuadKey)
                            {
                                if (!alerted.Contains(light.QuadKey))
                                {
                                    alerted.Add(light.QuadKey);
                                    Console.WriteLine(string.Format("lightning alert for {0}:{1}", light.AssetOwner, light.AssetName));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error encountered in processing data: {0} ", e.Message);
            }

            Console.WriteLine("--- Finished Generating Alert ---");
            Console.ReadLine();
        }
    }
}
