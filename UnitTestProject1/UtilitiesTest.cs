using LightningAlertConsoleApp;
using System.Text.Json;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System;
using NUnit.Framework;

namespace UnitTestProject1
{
    [TestFixture]
    public class UtilitiesTest
    {

        
        [Test]
        public void DeserializeTests()        
        {
            string lightningPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\..\..\TestData\lightning.json"));
            var result = (List<LightningStrike>)Utilities.Deserialize(lightningPath, typeof(List<LightningStrike>));
            Assert.AreEqual(typeof(List<LightningStrike>),result.GetType());
        }

        [Test]
        public void Deserialize_ThrowsJsonException_WhenInvalidJsonFormat()
        {
            string assetPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\..\..\TestData\assetsError.json"));
            Assert.Catch<JsonException>(() => Utilities.Deserialize(assetPath, typeof(List<Asset>)));

        }
        [Test]
        public void Deserialize_ReturnsNull_WhenFileDoesNotExist()
        {
            string assetPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\..\..\TestData\assetsNotThere.json"));
            var result = Utilities.Deserialize(assetPath,typeof(List<Asset>));
            Assert.IsNull(result);

        }

        [Test]
        [TestCase(0, 8.7020156, -12.2736188)]
        public void LatLongToQuadKey_ReturnNull_WhenLevelIsZero(int level, double lat, double lo) 
        {
           string result = Utilities.LatLongToQuadKey(level, lat, lo);
           Assert.IsNull(result);
        }

        [Test]
        [TestCase(25, 8.7020156, -12.2736188)]
        public void LatLongToQuadKey_ReturnNull_WhenLevelIsGreaterThan23(int level, double lat, double lo)
        {
            string result = Utilities.LatLongToQuadKey(level, lat, lo);
           Assert.IsNull(result);
        }

        [Test]
        [TestCase(12, 8.7020156, -12.2736188, "033321132300")]
        public void LatLongToQuadKeyTests(int level, double lat, double lo, string expected)
        {

            string result = Utilities.LatLongToQuadKey(level, lat, lo);
            Assert.AreEqual(result, expected);
        }
    }
}
