## All about the LightningAlertApp

1. Launch the LightningAlertApp.exe in ConsoleApp3\ConsoleApp3\bin\Debug\netcoreapp3.1.
2. The Console App will automatically display all the strikes.
3. Files to be read by the system is in a fixed location. For this exercise, It will be in ConsoleApp3\Data.
4. All errors found in the system will be logged in LightningAlert.log. It is located in ConsoleApp3\ConsoleApp3\bin\Debug\logs
5. UnitTestProject is also available when launching the LightningAlert.sln
6. For Test Methods that access files. The unit test reads the files from F:\Coding\ConsoleApp3\TestData.
---

